class Validation {
  String? sdtValidator(String sdt) {
    if (sdt.length < 0) {
      return "Không được để trống!";
    }
    if (!RegExp("^[0-9]*\$").hasMatch(sdt)) {
      return "Không đúng định dạng số điện thoại!";
    }
    if (sdt.length < 10) {
      return "Số điện thoại quá ngắn!";
    }
    if (sdt.length > 11) {
      return "Số điện thoại quá dài!";
    }
    return null;
  }

  String? passValidator(String pass) {
    if (pass.length < 8) {
      return "Mật khẩu trên 8 kí tự!";
    }
    return null;
  }

  String? nameValidator(String name) {
    if (name.isEmpty) {
      return "Không được phép bỏ trống";
    }
    if (!RegExp("^[A-z]").hasMatch(name)) {
      return "Không đúng định dạng tên";
    }
    return null;
  }
}
