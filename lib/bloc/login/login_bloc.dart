import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:login_screen/utils/validation.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitialSubmissionStatus());

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is LoginSubmitted) {
      yield* _handleLogin(event.sdt, event.pass);
    }
  }

  Stream<LoginState> _handleLogin(String sdt, String pass) async* {
    yield LoginProcessingSubmissionStatus();

    await Future.delayed(Duration(seconds: 2));
    try {
      if (Validation().sdtValidator(sdt) != null ||
          Validation().passValidator(pass) != null) {
        throw Exception("failed login");
      }

      final prefs = await SharedPreferences.getInstance();
      String _sdt = sdt;
      prefs.setString('sdt', _sdt);
      String _pass = pass;
      prefs.setString('pass', _pass);

      yield LoginSuccessSubmission();
    } on Exception catch (e) {
      yield LoginFailedSubmission(e);
    }
  }
}
