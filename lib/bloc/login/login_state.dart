part of 'login_bloc.dart';

abstract class LoginState extends Equatable {}

class LoginInitialSubmissionStatus extends LoginState {
  LoginInitialSubmissionStatus();
  @override
  List<Object> get props => [];
}

class LoginProcessingSubmissionStatus extends LoginState {
  LoginProcessingSubmissionStatus();
  @override
  List<Object> get props => [];
}

class LoginSuccessSubmission extends LoginState {
  LoginSuccessSubmission();
  @override
  List<Object> get props => [];
}

class LoginFailedSubmission extends LoginState {
  final Exception exception;

  LoginFailedSubmission(this.exception);
  @override
  List<Object> get props => [exception];
}
