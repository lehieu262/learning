part of 'login_bloc.dart';

abstract class LoginEvent {}

class LoginSubmitted extends LoginEvent {
  final String sdt;
  final String pass;
  LoginSubmitted({required this.sdt, required this.pass});
}
