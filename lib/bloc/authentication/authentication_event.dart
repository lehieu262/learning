part of 'authentication_bloc.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class AuthenticatedEvent extends AuthenticationEvent {
  @override
  List<Object> get props => [];
}

class LogOutEvent extends AuthenticationEvent {
  @override
  List<Object> get props => [];
}
