part of 'authentication_bloc.dart';

abstract class AuthenticationState extends Equatable {
  @override
  List<Object> get props => [];
}

class AuthenticatedState extends AuthenticationState {
  final String sdt;
  AuthenticatedState({required this.sdt});
  @override
  List<Object> get props => [this.sdt];
}

class UnauthenticatedState extends AuthenticationState {}
