import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:shared_preferences/shared_preferences.dart';
part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc() : super(UnauthenticatedState());

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AuthenticatedEvent) {
      yield* _handleLogin();
    } else if (event is LogOutEvent) {
      yield* _handleLogout();
    }
  }

  Stream<AuthenticationState> _handleLogin() async* {
    final prefs = await SharedPreferences.getInstance();
    String sdt = (prefs.getString('sdt') ?? " ");
    print(sdt);

    if (sdt != " ") {
      yield AuthenticatedState(sdt: sdt);
      print("success");
    } else {
      yield UnauthenticatedState();
      print("failed");
    }
  }

  Stream<AuthenticationState> _handleLogout() async* {
    final _prefs = await SharedPreferences.getInstance();
    _prefs.clear();

    yield UnauthenticatedState();
  }
}
