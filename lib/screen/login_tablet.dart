import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:login_screen/screen/sign_up_tablet.dart';

class LoginScreenTablet extends StatefulWidget {
  @override
  LoginScreenTabletState createState() => LoginScreenTabletState();
}

class LoginScreenTabletState extends State<LoginScreenTablet> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          alignment: Alignment.center,
          child: Column(
            children: [
              Expanded(
                flex: 0,
                child: Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(top: 130.0, left: 55.0),
                  child: Text(
                    "Welcome,",
                    style: GoogleFonts.openSans(
                      textStyle: TextStyle(
                          fontSize: 50.0,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(left: 55.0),
                  child: Text(
                    "Sign in to continue!",
                    style: GoogleFonts.openSans(
                      textStyle: TextStyle(
                          fontSize: 48.0,
                          color: Colors.grey,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: SizedBox(
                  height: 170.0,
                ),
              ),
              Expanded(
                flex: 0,
                child: Container(
                  width: 790.0,
                  alignment: Alignment.center,
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          validator: (String? email) {
                            if (email!.isEmpty) {
                              return "Không được phép bỏ trống";
                            }
                            if (!RegExp("^.*[@].*[.].*\$").hasMatch(email)) {
                              return "Không đúng dịnh dạng email";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            labelText: "Email ID",
                            labelStyle: TextStyle(fontSize: 26.0),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 35.0,
                        ),
                        TextFormField(
                          validator: (String? pass) {
                            if (pass!.length < 8) {
                              return "Mật khẩu ít nhất 8 kí tự";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            labelText: "Password",
                            labelStyle: TextStyle(fontSize: 26.0),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                          ),
                          obscureText: true,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: SizedBox(
                  height: 18.0,
                ),
              ),
              Expanded(
                flex: 0,
                child: Container(
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.only(right: 52.0),
                  child: Text(
                    "Forgot password?",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: SizedBox(
                  height: 120.0,
                ),
              ),
              Expanded(
                flex: 0,
                child: Container(
                  alignment: Alignment.center,
                  height: 70.0,
                  width: 790.0,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        if (_formKey.currentState!.validate()) {
                          return;
                        } else {
                          print("Không hợp lệ");
                        }
                      });
                    },
                    child: Ink(
                      height: 70.0,
                      width: 790.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [Colors.pinkAccent, Colors.orangeAccent],
                        ),
                      ),
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          "Login",
                          style: TextStyle(color: Colors.white, fontSize: 29.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: SizedBox(
                  height: 30.0,
                ),
              ),
              Expanded(
                flex: 0,
                child: Container(
                  height: 70.0,
                  width: 790.0,
                  decoration: BoxDecoration(
                    color: HexColor("#d8e3e6"),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                    ),
                    onPressed: null,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.facebook,
                          size: 40.0,
                          color: HexColor("#056c87"),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          "Connect with Facebook",
                          style: TextStyle(
                              color: HexColor("#056c87"),
                              fontSize: 27.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: SizedBox(
                  height: 170.0,
                ),
              ),
              Expanded(
                flex: 0,
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "I\'m a new user.",
                        style: TextStyle(
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SignUpScreenTablet()),
                          );
                        },
                        child: Text(
                          " Sign Up",
                          style: TextStyle(
                              fontSize: 25.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.pinkAccent),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
