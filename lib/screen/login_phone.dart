import 'dart:ui';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:login_screen/bloc/authentication/authentication_bloc.dart';
import 'package:login_screen/bloc/login/login_bloc.dart';
import 'package:login_screen/screen/home_page.dart';
import 'package:login_screen/utils/validation.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _sdtController = new TextEditingController();
  final _passController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              _textWelcome(),
              _textSignin(),
              _inputForm(),
              _textForgetPass(),
              _blocBuilderLoginButton(),
              _fbButton(),
              _textSignup(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _textWelcome() {
    return Container(
      alignment: Alignment.topLeft,
      margin: EdgeInsets.only(top: 50, left: 25),
      child: Text(
        "Welcome,",
        style: GoogleFonts.openSans(
          textStyle: TextStyle(
              color: Colors.black, fontSize: 30.0, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget _textSignin() {
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.only(top: 10, left: 25.0),
      child: Text(
        "Sign in to continue!",
        style: GoogleFonts.openSans(
          textStyle: TextStyle(
            color: Colors.grey,
            fontSize: 26.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Widget _formField() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          _inputFormsdt(),
          _inputFormPass(),
        ],
      ),
    );
  }

  Widget _inputFormsdt() {
    return Container(
      margin: EdgeInsets.only(top: 80),
      child: TextFormField(
        validator: (sdt) => Validation().sdtValidator(sdt!),
        controller: _sdtController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: "Số điện thoại",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
      ),
    );
  }

  Widget _inputFormPass() {
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: TextFormField(
        validator: (pass) => Validation().passValidator(pass!),
        controller: _passController,
        decoration: InputDecoration(
          labelText: "Password",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
        obscureText: true,
      ),
    );
  }

  Widget _textForgetPass() {
    return Container(
      margin: EdgeInsets.only(top: 20),
      alignment: Alignment.centerRight,
      padding: EdgeInsets.only(right: 26.0),
      child: Text(
        "Forget Password?",
        style: TextStyle(
            color: Colors.black, fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
    );
  }

  Widget _loginButton() {
    return Container(
      margin: EdgeInsets.only(top: 50),
      width: 360.0,
      height: 58.0,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          padding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
        onPressed: () {
          setState(
            () {
              if (_formKey.currentState!.validate()) {
                context.read<LoginBloc>().add(
                      LoginSubmitted(
                          sdt: _sdtController.text, pass: _passController.text),
                    );
              } else {
                print("Không hợp lệ");

                context.read<LoginBloc>().add(
                      LoginSubmitted(
                          sdt: _sdtController.text, pass: _passController.text),
                    );
              }
            },
          );
        },
        child: Ink(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [Colors.pinkAccent, Colors.orangeAccent],
            ),
          ),
          child: Container(
            alignment: Alignment.center,
            child: Text(
              "Login",
              style: TextStyle(fontSize: 18.0, color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }

  Widget _fbButton() {
    return Container(
      margin: EdgeInsets.only(top: 15),
      width: 360.0,
      height: 58.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: HexColor("#d8e3e6"),
      ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
        onPressed: null,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.facebook,
              size: 30.0,
              color: HexColor("#056c87"),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              'Connect with Facebook',
              style: TextStyle(
                  color: HexColor("#056c87"),
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }

  Widget _textSignup() {
    return Container(
      margin: EdgeInsets.only(top: 110),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'I\'m a new user.',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 17.0),
          ),
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/signup');
            },
            child: Text(
              ' Sign Up',
              style: TextStyle(
                  color: Colors.pinkAccent,
                  fontWeight: FontWeight.bold,
                  fontSize: 17.0),
            ),
          ),
        ],
      ),
    );
  }

  Widget _inputForm() {
    return Container(
      width: 360,
      child: BlocListener<LoginBloc, LoginState>(
        listener: (context, state) {
          final submissionStatus = state;
          if (submissionStatus is LoginFailedSubmission) {
            _showSnackBar(context);
          } else if (submissionStatus is LoginSuccessSubmission) {
            context.read<AuthenticationBloc>().add(AuthenticatedEvent());
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => HomePage(),
              ),
            );
          }
        },
        child: _formField(),
      ),
    );
  }

  Widget _blocBuilderLoginButton() {
    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        return state is LoginProcessingSubmissionStatus
            ? CircularProgressIndicator()
            : _loginButton();
      },
    );
  }

  void _showSnackBar(BuildContext context) {
    final snackBar = SnackBar(
      duration: Duration(seconds: 1),
      behavior: SnackBarBehavior.fixed,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      backgroundColor: HexColor("#878382"),
      content: Text(
        "Đăng nhập thất bại!",
        style: TextStyle(color: Colors.white, fontSize: 18),
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
