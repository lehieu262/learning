import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:login_screen/bloc/authentication/authentication_bloc.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var content = Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 30),
                width: 360,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 3.0,
                    color: Colors.blueAccent,
                  ),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Column(
                  children: [
                    _textEmail(),
                    _textFullName(),
                    _textAddress(),
                  ],
                ),
              ),
              _elevatedButton(context),
            ],
          ),
        ),
      ),
    );
    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (context, state) {
        if (state is UnauthenticatedState) {
          Navigator.pushNamed(context, '/login');
        }
      },
      child: content,
    );
  }

  Widget _textEmail() {
    return BlocBuilder<AuthenticationBloc, AuthenticationState>(
      builder: (context, state) {
        if (state is AuthenticatedState) {
          return Container(
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(vertical: 20),
            child: Text(
              "Email: ${state.sdt}",
              style: TextStyle(fontSize: 18),
            ),
          );
        }
        return Container();
      },
    );
  }

  Widget _textFullName() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      child: Text(
        "Full Name: Le Chien",
        style: TextStyle(fontSize: 18),
      ),
    );
  }

  Widget _textAddress() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      child: Text(
        "Address: Viet Nam",
        style: TextStyle(fontSize: 18),
      ),
    );
  }

  Widget _elevatedButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 100),
      child: ElevatedButton(
        child: Text("Đăng xuất"),
        onPressed: () {
          context.read<AuthenticationBloc>().add(
                LogOutEvent(),
              );
        },
      ),
    );
  }
}
