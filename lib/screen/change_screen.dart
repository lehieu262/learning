import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:login_screen/bloc/authentication/authentication_bloc.dart';
import 'package:login_screen/screen/home_page.dart';
import 'package:login_screen/screen/login_phone.dart';
import 'package:login_screen/screen/login_tablet.dart';

class ChangeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthenticationBloc, AuthenticationState>(
      bloc: context.read<AuthenticationBloc>()..add(AuthenticatedEvent()),
      builder: (context, state) {
        return LayoutBuilder(
          builder: (context, constraints) {
            print(constraints.maxHeight);
            if (constraints.maxHeight > 830) {
              return LoginScreenTablet();
            } else {
              if (state is UnauthenticatedState) {
                return LoginScreen();
              }
              return HomePage();
            }
          },
        );
      },
    );
  }
}
