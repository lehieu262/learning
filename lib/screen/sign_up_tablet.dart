import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class SignUpScreenTablet extends StatefulWidget {
  @override
  SignUpScreenTabletState createState() => SignUpScreenTabletState();
}

class SignUpScreenTabletState extends State<SignUpScreenTablet> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          alignment: Alignment.center,
          child: Column(
            children: [
              Expanded(
                flex: 0,
                child: Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(top: 130.0, left: 55.0),
                  child: Text(
                    "Create Account,",
                    style: GoogleFonts.openSans(
                      textStyle: TextStyle(
                          fontSize: 50.0,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(left: 55.0),
                  child: Text(
                    "Sign up to get started!",
                    style: GoogleFonts.openSans(
                      textStyle: TextStyle(
                          fontSize: 48.0,
                          color: Colors.grey,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: SizedBox(
                  height: 140.0,
                ),
              ),
              Expanded(
                flex: 0,
                child: Container(
                  width: 790.0,
                  alignment: Alignment.center,
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          validator: (String? name) {
                            if (name!.isEmpty) {
                              return "Không được phép bỏ trống";
                            }
                            if (!RegExp("^[A-z]").hasMatch(name)) {
                              return "Không đúng định dạng tên";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            labelText: "Full Name",
                            labelStyle: TextStyle(fontSize: 26.0),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 35.0,
                        ),
                        TextFormField(
                          validator: (String? email) {
                            if (email!.isEmpty) {
                              return "Không được phép bỏ trống";
                            }
                            if (!RegExp("^.*[@].*[.].*\$").hasMatch(email)) {
                              return "Không đúng dịnh dạng email";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            labelText: "Email ID",
                            labelStyle: TextStyle(fontSize: 26.0),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 35.0,
                        ),
                        TextFormField(
                          validator: (String? pass) {
                            if (pass!.length < 8) {
                              return "Mật khẩu ít nhất 8 kí tự";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            labelText: "Password",
                            labelStyle: TextStyle(fontSize: 26.0),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                          ),
                          obscureText: true,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: SizedBox(
                  height: 120.0,
                ),
              ),
              Expanded(
                flex: 0,
                child: Container(
                  alignment: Alignment.center,
                  height: 70.0,
                  width: 790.0,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        if (_formKey.currentState!.validate()) {
                          return;
                        } else {
                          print("Không hợp lệ");
                        }
                      });
                    },
                    child: Ink(
                      height: 70.0,
                      width: 790.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [Colors.pinkAccent, Colors.orangeAccent],
                        ),
                      ),
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          "Login",
                          style: TextStyle(color: Colors.white, fontSize: 29.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: SizedBox(
                  height: 30.0,
                ),
              ),
              Expanded(
                flex: 0,
                child: Container(
                  height: 70.0,
                  width: 790.0,
                  decoration: BoxDecoration(
                    color: HexColor("#d8e3e6"),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                    ),
                    onPressed: null,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.facebook,
                          size: 40.0,
                          color: HexColor("#056c87"),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          "Connect with Facebook",
                          style: TextStyle(
                              color: HexColor("#056c87"),
                              fontSize: 27.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: SizedBox(
                  height: 170.0,
                ),
              ),
              Expanded(
                flex: 0,
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "I\'m already a member.",
                        style: TextStyle(
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          " Sign In",
                          style: TextStyle(
                              fontSize: 25.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.pinkAccent),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
