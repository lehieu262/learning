import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:login_screen/bloc/authentication/authentication_bloc.dart';
import 'package:login_screen/screen/change_screen.dart';
import 'package:login_screen/screen/home_page.dart';
import 'package:login_screen/screen/login_phone.dart';
import 'package:login_screen/screen/sign_up_phone.dart';

import 'bloc/login/login_bloc.dart';

void main() {
  runApp(MultiBlocProvider(
    providers: [
      BlocProvider(
        create: (context) => AuthenticationBloc(),
      ),
      BlocProvider(
        create: (context) => LoginBloc(),
      ),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: '/',
      routes: {
        '/': (context) => ChangeScreen(),
        '/login': (context) => LoginScreen(),
        '/homepage': (context) => HomePage(),
        '/signup': (context) => SignUpScreen(),
      },
    );
  }
}
